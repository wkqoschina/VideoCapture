package com.bingo.subscription;

public interface Subscription {
    void unsubscribe();
    boolean isUnsubscribed();
}
