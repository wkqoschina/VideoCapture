package ga.iloveyaya.easyipcam;

public class IPCameraSDK {
    private static boolean isLoaded = false;

    static {
        if (!isLoaded) {
            System.loadLibrary("HancNetSDK");
            //System.loadLibrary("easycam");
            isLoaded = true;
        }
    }

    public IPCameraSDK() {

    }

    public boolean init() {
        return nativeInit();
    }

    public int connect(String szIP, int nPort, String pLinkCmd, int nTimeOut, long dwUser) {
        return nativeConnect(szIP, nPort, pLinkCmd, nTimeOut, dwUser);
    }

    // callback
    public void NetCallback(int lUserID, byte []pBuf, int nBufLen, long dwUser) {

    }

    private final native boolean nativeInit();

    private final native int nativeConnect(String szIP, int nPort, String pLinkCmd, int nTimeOut, long dwUser);

    private final native boolean nativeDisConnect(int nSession, String pStopCmd);

    private final native boolean nativeSendData(int nSession ,byte []pData, int nLen);

    private final native int nativeCommunicateWithServerTcp(String szIP, int nPort, byte []pSendBuf, int nSendLen);
    //private final native int nativeCommunicateWithServerTcp(String szIP, int nPort, byte []pSendBuf, int nSendLen, char **ppRecvBuf,int & pRecvLen,
    //                                        int nTimeOut = 3000, boolean bGetData = false);

    private final native boolean nativeDataRelease(int nSession);

    private final native int nativeConnectEx(String ip, int port, byte []serial, int len, int timeout, long dwUser);
    private final native boolean nativeSendEx(int session, int type, byte []data, int len);
    private final native boolean nativeDisConnectEx(int session);
}
