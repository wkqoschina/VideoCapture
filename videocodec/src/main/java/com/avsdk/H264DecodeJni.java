package com.avsdk;

public class H264DecodeJni {
    static {
        Load();
    }
    private static  H264DecodeJni _h264;

    public static H264DecodeJni getInstance(){
        if(_h264==null){
            _h264=new H264DecodeJni();
        }
        return  _h264;
    }
    private static void Load(){
        System.loadLibrary("hancnv");
    }
    //只要有编码数据，就要调用H264DecodeJni_InputData输入数据

    public native int InputData(int handle,int intype,byte[] indata,int inlength);
    //(不需要输入文件名，只需要输入录像目录)
    public  native  boolean StartRecord(int handle,byte[] infilename);
   //点击停止录像按钮 H264DecodeJni_StopRecord
    public  native void StopRecord(int handle);
    public native int InitDecode(int width,int height);
    //每次新连接发个头
    public native int GetVideoHead(int handle, byte[] outdata);
    //
    public native int DecodeVideo(int dechandle,byte[] indata, int inLength,byte[] outdata, int outLength);

    //预览界面摧毁，H264DecodeJni_UnInitDecode
    public native void UnInitDecode(int playhandle);

    //画面旋转  mode=0 不转  model=1 90度  2 倒置  3
    public native boolean RotateYuv(byte[] indata,int width,int height,byte[] outdata,int mode);


    //画面旋转
    public native boolean RotateNV(byte[] indata,int width,int height,byte[] outdata,int mode);




    /*
    * if(nType == 0)
 {
     bSucc = NV12ToYV12(pSou,pDes,nWidth,nHeight) ;
 }
 else if(nType == 1)
 {
  bSucc = NV21ToYV12(pSou,pDes,nWidth,nHeight) ;
 }
 else if(nType == 2)
 {
  bSucc = YV12ToNV12(pSou,pDes,nWidth,nHeight);
 }
 else if(nType == 4)
 {
  bSucc = YV12ToNV21(pSou,pDes,nWidth,nHeight);
 }
    *
    * */
    //NV 转化成YUV
    public native boolean NvToYuv(int type,byte[] indata,int width,int height,byte[] outdata);


    //初始化网络
    public native  boolean NetInit();
    //连接上位机，无需像以前一样用线程连接，内部自己处理 返回-1失败 >=0 成功
    public native  int ConnectEx(Byte[] ip,int port,Byte[] indata,int len,int timeout);
    /*发送数据，只要连上就直接往上发数据，无需再判断访问和通断
    // jint session： ConnectEx返回值；
    // jint type ：数据类型,2,视频，10音频
    // jbyteArray indata：编码后数据
    // jint len：数据长度
     */
    public native  boolean SendEx(int session,int type,Byte[] indata,int len);

    //关闭连接，当改变地址端口或序列时调用
    public native  boolean DisConnectEx(int session);
}
