package com.avsdk;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.os.SystemClock;

import java.io.IOException;

/**
 * 视频编码
 */
public class VideoEncode {
    private long mTimestampStartMs = 0;
    private long mTimestampDeltaMs = 0;
    private long mTimestampCurrentMs = 0;
    private long mTimestampLastMs = 0;

    private MediaVideoEncoderHW mVideoEncoder = null;
    int nWidth = 1280;
    int nHeight = 720;

    public void InitEncode(int width, int height, int KBps) throws IOException {
        nWidth = width;
        nHeight = height;

        //  android版本手机硬编码是不同的，（需要区分设置）
        int supportColorFormat = GetSupportColorFormat();
        mVideoEncoder = new MediaVideoEncoderHW(width, height, supportColorFormat, KBps);
        mVideoEncoder.start();
    }

    ;

    public int EncodeVideo(byte[] indata, int inLength, byte[] outdata, int outLength) {
        if (mTimestampStartMs == 0) {
            mTimestampStartMs = SystemClock.elapsedRealtimeNanos() / 1000000;
            mTimestampCurrentMs = mTimestampStartMs;
            mTimestampLastMs = mTimestampStartMs;
        } else {
            mTimestampCurrentMs = SystemClock.elapsedRealtimeNanos() / 1000000;
            mTimestampDeltaMs = mTimestampCurrentMs - mTimestampLastMs;
            mTimestampLastMs = mTimestampCurrentMs;
        }

        BufferUnit bufUnit = new BufferUnit();

        bufUnit.setData(indata);


        bufUnit.setLength(nWidth * nHeight * 3 / 2);
        bufUnit.setPts(mTimestampCurrentMs - mTimestampStartMs);


        mVideoEncoder.setInput(bufUnit);
        BufferUnit bufUnitEncode = new BufferUnit();
        bufUnitEncode.setData(new byte[nWidth * nHeight * 3 / 2]);
        int outsize = mVideoEncoder.getOutput(bufUnitEncode, outdata);
        return outsize;
    }
    public boolean IsEncodeOpen() {
        return mVideoEncoder != null;
    }

    public void UnInitEncode() {
        if (mVideoEncoder != null) {
            mVideoEncoder.stop();
            mVideoEncoder = null;
        }
    }


    /*
     * 获取MediaCodec 支持的格式Color
     * */
    public int GetSupportColorFormat() {
        int numCodecs = MediaCodecList.getCodecCount();
        MediaCodecInfo codecInfo = null;
        for (int i = 0; (i < numCodecs) && (codecInfo == null); i++) {
            MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);
            if (info.isEncoder()) {
                String types[] = info.getSupportedTypes();
                boolean found = false;
                for (int j = 0; (j < types.length) && (!found); j++) {
                    if (types[j].equals("video/avc")) {
                        found = true;
                    }
                }
                if (found) {
                    codecInfo = info;
                }
            }
        }
        MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType("video/avc");
        int j[] = capabilities.colorFormats;
        for (int types = 0; types < j.length; types++) {
            int colorFormat = j[types];
            switch (colorFormat) {
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
                case MediaCodecInfo.CodecCapabilities.COLOR_QCOM_FormatYUV420SemiPlanar:
                    return colorFormat;
            }
        }
        return -1;
    }
}
