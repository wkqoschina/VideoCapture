package com.avsdk;

public class AudioEncode {
	
	private AudioEncDecJni instance = null;	
		
	
	public void InitEncode()
	{
		if(instance==null){
			instance=AudioEncDecJni.getInstance();
		}		
		instance.InitEncode();				
	};

	public int EncodeAudio(byte[] indata, int inLength,byte[] outdata, int outLength)
	{
		if(instance==null){
			instance=AudioEncDecJni.getInstance();
		}		
		return instance.EncodeAudio(indata, inLength, outdata, outLength);
	};	
	
	public void UnInitEncode()
	{
		if(instance==null){
			instance=AudioEncDecJni.getInstance();
		}		
		instance.UnInitEncode();
	};		
}
