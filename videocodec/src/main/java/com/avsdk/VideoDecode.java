package com.avsdk;

public class VideoDecode {
	private H264DecodeJni decinstance = null;	
	int dechandle;	
	public void InitDecode(int width,int height)
	{
		if(decinstance==null){
			decinstance=H264DecodeJni.getInstance();
		}		
		dechandle = decinstance.InitDecode(width, height);		
	};

	public int DecodeVideo(byte[] indata, int inLength,byte[] outdata, int outLength)
	{
		if(decinstance==null){
			decinstance=H264DecodeJni.getInstance();
		}			
		int ret = 0;
		ret = decinstance.DecodeVideo(dechandle, indata, inLength, outdata, outLength);		
		return ret;
	};	
	
	public void UnInitDecode()
	{
		if(decinstance==null){
			decinstance=H264DecodeJni.getInstance();
		}			
		decinstance.UnInitDecode(dechandle);		
	};		
	
	

}
