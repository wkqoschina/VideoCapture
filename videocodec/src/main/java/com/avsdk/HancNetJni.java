package com.avsdk;

public class HancNetJni {

    private  static  HancNetJni _hancNet;
    static {
        Load();
    }
    private static void Load()
    {
        System.loadLibrary("HancNetSDK");
        System.loadLibrary("hancnv");
    }
    public static HancNetJni getInstance(){
        if(_hancNet==null){
            _hancNet=new HancNetJni();
        }
        return  _hancNet;
    }

    //初始化网络
    public native  boolean HancNetInit();
    //连接上位机，无需像以前一样用线程连接，内部自己处理 返回-1失败 >=0 成功
    public native  int HancConnectEx(byte[] ip,int port,byte[] indata,int len,int timeout);
    /*发送数据，只要连上就直接往上发数据，无需再判断访问和通断
    // jint session： ConnectEx返回值；
    // jint type ：数据类型,2,视频，10音频
    // jbyteArray indata：编码后数据
    // jint len：数据长度
     */
    public native  boolean HancSendEx(int session,int type,byte[] indata,int len);

    //关闭连接，当改变地址端口或序列时调用
    public native  boolean HancDisConnectEx(int session);

}
