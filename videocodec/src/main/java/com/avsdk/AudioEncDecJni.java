package com.avsdk;


public class AudioEncDecJni {
	
	private static AudioEncDecJni instance=null;
	private AudioEncDecJni(){
	}
	public static AudioEncDecJni getInstance(){
		if(instance==null){
			instance =new AudioEncDecJni();
		}
		return instance;
	}	

	public native void InitEncode();

	public native int EncodeAudio(byte[] indata, int inLength,byte[] outdata, int outLength);	
	
	public native void UnInitEncode();			
	
	
	public native void InitDecode();

	public native int DecodeAudio(byte[] indata, int inLength,byte[] outdata, int outLength);	
	
	public native void UnInitDecode();		
}
