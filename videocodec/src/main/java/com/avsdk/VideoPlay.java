package com.avsdk;


import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;








import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;








import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class VideoPlay {


    static public int m_width = 1280;
    static public int m_height = 720;





    //来自网络端的视频预览和预览控制器
    SurfaceView surfaceView_remote=null;
    SurfaceHolder holder_remote=null;








    //接收每一帧视频的缓存——该缓存是时刻在存入和写出，需要对其加锁

    private int bytesPerPackage3;

    private byte[] videoBufferRemote;



    //视频贴图的画布
    private Canvas mCanvas=null;
    Bitmap bt3=null;
    BitmapFactory.Options opt3;
    boolean bDraw = false;
    boolean mbfront;

    public void StartPlay(SurfaceView view,boolean bfront,int width,int height)
    {
        bDraw = true;

        m_width = width;
        m_height = height;
        bytesPerPackage3=m_width*m_height*2;
        videoBufferRemote=new byte[bytesPerPackage3];

        //显示通话端视频的SurfaceView
        surfaceView_remote = view;
        holder_remote = surfaceView_remote.getHolder();
        holder_remote.setFormat(PixelFormat.TRANSLUCENT);


        opt3=new BitmapFactory.Options();
        //设定BitmapFactory.Options对象特性
        opt3.inPreferredConfig=Config.RGB_565;
        opt3.outWidth=m_width;
        opt3.outHeight=m_height;
        bt3=Bitmap.createBitmap(opt3.outWidth, opt3.outHeight, opt3.inPreferredConfig);
        if(bt3 == null)
        {
            Log.d("play","createBitmap false");
        }



        mbfront = bfront;




    }

    public void WritePlayBuffer(byte[] playbuffer, int len)
    {
        if(playbuffer==null||len==0)return; //2020-04-23 xamarin调用可能为Null
        System.arraycopy(playbuffer, 0, videoBufferRemote,0, len);


        ByteBuffer buffer = ByteBuffer.wrap( videoBufferRemote,0, bytesPerPackage3);//将 byte 数组包装到缓冲区中
        bt3.copyPixelsFromBuffer(buffer);
        Message msg;
        msg=myHandler.obtainMessage(0xAA,bytesPerPackage3,0);



        //发送消息
        myHandler.sendMessage(msg);
    }



    Handler myHandler=new Handler(){
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            switch(msg.what){
                case 0xAA:
                    synchronized (bt3) {
                        Log.d("play", "收到了绘制消息，即将绘制一帧bitmap");
                        if(bDraw)
                            drawBmp(holder_remote,msg.arg1,msg.arg2);

                    }
            }
        }
    };

    //绘图函数
    private void drawBmp(SurfaceHolder holder,int bytesNum,int rotate){


        if(bytesNum == bytesPerPackage3)
        {


            Rect viewrect = holder.getSurfaceFrame();
            if ((viewrect.width() <= 0) || (viewrect.height() <= 0))
                return;
            Rect localrect = new Rect();

            localrect.left = 0;
            localrect.top = 0;

            localrect.right = m_width;
            localrect.bottom = m_height;





            Rect paintrect = new Rect();
            if((viewrect.width()*1000/localrect.right) > (viewrect.height()*1000/localrect.bottom))
            {
                paintrect.top = viewrect.top;
                paintrect.bottom = viewrect.bottom;
                paintrect.left = viewrect.left + (viewrect.width() - (localrect.right * viewrect.height()*1000/localrect.bottom)/1000)/2;
                paintrect.right = viewrect.left + paintrect.left +  (localrect.right * viewrect.height()*1000/localrect.bottom)/1000;
            }else{
                paintrect.left = viewrect.left;
                paintrect.right = viewrect.right;
                paintrect.top = viewrect.top + (viewrect.height() - (localrect.bottom * viewrect.width()*1000/localrect.right)/1000)/2;
                paintrect.bottom = viewrect.top + paintrect.top + (localrect.bottom * viewrect.width()*1000/localrect.right)/1000;

            }




            try {
                mCanvas = holder.lockCanvas();
                if(mCanvas == null)
                    return ;
                // 将bitmap绘制到canvas依附的缓存——surfaceview自己的默认缓存区 之中
	/*
			Paint paint = new Paint();

			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
			mCanvas.drawPaint(paint);
			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));


			int flags = paint.getFlags();

			paint.setFlags(flags | Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
	*/
                if(mCanvas != null)
                {

                    if(mbfront)
                    {
                        Matrix vMatrix = new Matrix();


                        //		vMatrix.postRotate(0);
                        vMatrix.postScale(-1,1);

                        Bitmap vB2 = Bitmap.createBitmap(bt3, 0, 0, bt3.getWidth() // 宽度
                                , bt3.getHeight() // 高度
                                , vMatrix, true);

                        mCanvas.drawBitmap(vB2, localrect, paintrect, null);// 这里画的是旋转后的
                    }else{
                        mCanvas.drawBitmap(bt3, localrect, paintrect, null);
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }
            finally{
                if(mCanvas != null)
                    holder.unlockCanvasAndPost(mCanvas);
            }
        }

    }







    public void StopPlay()
    {

    }


}
