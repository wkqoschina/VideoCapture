package com.avsdk;

public class AudioDecode {
	private AudioEncDecJni instance = null;	
		
	
	public void InitDecode()
	{
		if(instance==null){
			instance=AudioEncDecJni.getInstance();
		}		
		instance.InitDecode();				
	};

	public int DecodeAudio(byte[] indata, int inLength,byte[] outdata, int outLength)
	{
		if(instance==null){
			instance=AudioEncDecJni.getInstance();
		}		
		return instance.DecodeAudio(indata, inLength, outdata, outLength);
	};	
	
	public void UnInitDecode()
	{
		if(instance==null){
			instance=AudioEncDecJni.getInstance();
		}		
		instance.UnInitDecode();
	};		
}
