package com.avsdk;



import android.content.Context;
import android.media.AudioManager;
import android.util.Log;



public class AudioCap {
	

	private Thread recordAudioThread;

	audioRecordPlay audio = null;


	private Runnable recordAudioProc = new Runnable() {
		public void run() {

			audio.recordAudio();
		}
	};


	
	
	
	public void StartCapture(AVCapCallBack avcapcallback)
	{
		audio=new audioRecordPlay();
		audio.SetCallback(avcapcallback);
		recordAudioThread = new Thread(recordAudioProc);
		recordAudioThread.start();

	}


	
	public void StopCapture()
	{

		audio.isRecording=false;
		while(audio.capfinish == false)
		{
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	
	}
}
