package com.wu.capture.ui

import android.Manifest
import android.app.Dialog
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.wkq.base.frame.activity.MvpBindingActivity
import com.wu.capture.R
import com.wu.capture.frame.presenter.LaunchPresenter
import com.wu.capture.frame.view.LaunchView


/**
 * @author wkq
 *
 * @date 2022年02月24日 9:07
 *
 *@des
 *
 */

class LaunchActivity:MvpBindingActivity<LaunchView, LaunchPresenter,ViewDataBinding>() {

    //权限Dialog
    var dialog: Dialog?=null
    //申请的权限
    var permissionsREAD = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA)
    //权限Code
    var REQUEST_CODE_LAUNCH = 10011


    override fun getLayoutId(): Int {
        return R.layout.activity_launch
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view.initView()

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_LAUNCH) {
            val hasPermissions = getPresenter().onRequestPermissionsResult(this, requestCode, permissions, grantResults)
            if (hasPermissions!![0]) {
                if (mvpView != null) mvpView.checkPermissions()
            } else {
                if (hasPermissions[1]) {
                    mvpView.showPermissionPerpetual(requestCode)
                } else {
                    if (mvpView != null) mvpView.checkPermissions()
                }
            }
        }
    }


}