package com.wu.capture.application

import android.util.Log
import androidx.multidex.MultiDexApplication
import com.avsdk.H264DecodeJni
import com.avsdk.HancNetJni
import com.avsdk.VideoCap
import com.avsdk.VideoEncode
import com.wu.base.util.AlertUtil
import com.wu.capture.util.PathUtil
import com.wu.capture.util.VideoCaptureConstant


/**
 * @author wkq
 *
 * @date 2022年02月23日 14:27
 *
 *@des
 *
 */

class VideoCaptureApplication : MultiDexApplication() {
    companion object{
         var netCode = -1
        var videoencBuffer:ByteArray?=null
        var videoCap: VideoCap? = null
        var videoEncode: VideoEncode? = null
        var initDecodeResult = -1

        //宽
        var previdewSize_Width = 0

        //高
        var getPrevidewSize_Height = 0

    }

    override fun onCreate() {
        super.onCreate()

        //加载媒体库
        loadMediaModule()
        //初始化网络库
        loadHencNetModule()
    }



    fun getNetCode(){

    }

    //初始化网络库
    fun loadHencNetModule() {

        try {
            HancNetJni.getInstance().HancNetInit();
            //地址
            var path = PathUtil.getPath(this)

            val inputTypes: ByteArray = VideoCaptureConstant.code.toByteArray()
            netCode = HancNetJni.getInstance().HancConnectEx(
                    VideoCaptureConstant.serviceIp.toByteArray(),
                    VideoCaptureConstant.port,
                    inputTypes,
                    inputTypes.size,
                    3000
            )

            Log.e("数据:", "appSetting.ServerIp:" + VideoCaptureConstant.serviceIp + " Code:" + VideoCaptureConstant.code)

        } catch (e: Exception) {
            AlertUtil.showDeftToast(this, "初始化网络库异常")

        }

    }


    //加载媒体库
    fun loadMediaModule() {
        try {
            videoCap = VideoCap()
            videoEncode = VideoEncode()
            initDecodeResult=   H264DecodeJni.getInstance().InitDecode(
                    getPrevidewSize_Height,
                    previdewSize_Width
            )
        } catch (e: Exception) {
            AlertUtil.showDeftToast(this, "初始化媒体库异常")

        }

    }


}