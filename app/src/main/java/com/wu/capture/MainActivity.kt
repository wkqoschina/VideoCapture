package com.wu.capture

import android.os.Bundle
import com.wkq.base.frame.activity.MvpBindingActivity
import com.wu.capture.databinding.ActivityMainBinding
import com.wu.capture.frame.presenter.MainPresenter
import com.wu.capture.frame.view.MainView

class MainActivity : MvpBindingActivity<MainView, MainPresenter,ActivityMainBinding>(){


    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        view.initView()
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onDestroy() {
        super.onDestroy()

    }

}