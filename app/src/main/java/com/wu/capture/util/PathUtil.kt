package com.wu.capture.util

import android.content.Context
import android.os.Build
import android.os.Environment
import java.io.File

object PathUtil {

    fun getPath(context: Context): String? {
        var fileName: String?
        fileName = if (Build.VERSION.SDK_INT >= 29) {
            context.getExternalFilesDir("")!!.absolutePath
        } else {
            Environment.getExternalStorageDirectory().path+"/Capture/"
        }
        val file = File(fileName)
        return if (!file.exists() && file.mkdirs()) {
            fileName
        } else fileName
    }

    fun getParentPath(context: Context): String? {
        var fileName: String?
        fileName = if (Build.VERSION.SDK_INT >= 29) {
            context.getExternalFilesDir("")!!.absolutePath+"/Video/"
        } else {
            Environment.getExternalStorageDirectory().path+ "/Video/"
        }
        val file = File(fileName)
        return if (!file.exists() && file.mkdirs()) {
            fileName
        } else fileName
    }
}