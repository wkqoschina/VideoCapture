package com.wu.capture.util

/**
 * 视频采集常量
 */
object VideoCaptureConstant {
    //类型
     const val PIX_FMT_YUV420P = 0 // yyyyuuvv

     const val PIX_FMT_YV12 = 1 // yyyyvvuu


    //视频宽高
    var videoWidth = 1280
    var videoHeight = 720
    var code="000000"
    var port=8020
    var serviceIp : String="192.168.0.233"

}