package com.wu.capture.frame.presenter

import com.wkq.base.frame.mosby.MvpBasePresenter
import com.wu.capture.frame.view.MainView


/**
 * @author wkq
 *
 * @date 2022年02月23日 15:15
 *
 *@des
 *
 */

class MainPresenter :MvpBasePresenter<MainView>() {


}