package com.wu.capture.frame.view

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.view.View
import android.view.WindowManager
import androidx.core.app.ActivityCompat
import com.wkq.base.frame.mosby.delegate.MvpView
import com.wu.base.util.AlertUtil
import com.wu.base.util.StatusBarUtil
import com.wu.capture.MainActivity
import com.wu.capture.R
import com.wu.capture.ui.LaunchActivity
import com.wu.capture.util.AlertDialogUtils
import com.wu.capture.util.PermissionChecker
import com.wu.capture.util.TimerWaitUtil


/**
 * @author wkq
 *
 * @date 2022年02月24日 9:09
 *
 *@des
 *
 */

class LaunchView(mActivity:LaunchActivity):MvpView {

    var mActivity=mActivity

    fun initView(){
        processFullScreen()
        checkPermissions()
    }


    /**
     *   处理刘海屏全屏问题
     */
    fun processFullScreen() {
        StatusBarUtil.setTransparentForWindow(mActivity)
        StatusBarUtil.addTranslucentView(mActivity, 0)
        StatusBarUtil.setLightMode(mActivity)

        val lp: WindowManager.LayoutParams = mActivity.getWindow().getAttributes()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lp.layoutInDisplayCutoutMode =
                WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES
        }
        mActivity.getWindow().setAttributes(lp)
        // 设置页面全屏显示
        val decorView: View = mActivity.getWindow().getDecorView()
        decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    }
    fun checkPermissions() {
        val hasPermission: Boolean = mActivity.getPresenter().checkPermissions(
            mActivity, mActivity.permissionsREAD,
            mActivity.REQUEST_CODE_LAUNCH
        )
        if (hasPermission) {
            mActivity.startActivity(Intent(mActivity,MainActivity().javaClass))
//            var timerUtil = TimerWaitUtil()
//            timerUtil!!.totalTime = 2000
//            timerUtil!!.intervalTime = 100
//            timerUtil!!.setTimerLiener(object : TimerWaitUtil.TimeListener {
//                override fun onFinish() {
//
//                }
//
//
//                override fun onInterval(time: Long) {
//                }
//
//            })
//            timerUtil!!.start()
        }
    }


    fun showPermissionPerpetual(requestCode: Int) {
        if (mActivity.dialog != null) mActivity.dialog?.dismiss()
        mActivity.dialog = AlertDialogUtils.showTwoButtonDialog(
            mActivity,
            "取消",
            "去设置",
            "你的手机没有授权软件权限,将无法正常使用!", R.color.color_dialog_btn,
            R.color.color_ffa300, object : AlertDialogUtils.DialogTwoListener {
                override fun onClickLeft(dialog: Dialog?) {
                    dialog?.dismiss()
                    showMessage("无法获取存读取权限,您的app将无法正常使用")
                    mActivity.finish()
                }

                override fun onClickRight(dialog: Dialog?) {
                    dialog?.dismiss()
                    PermissionChecker.settingPermissionActivity(mActivity, requestCode)
                }
            })
    }


    fun showPermission(needList: List<String>, requestCode: Int) {
        if (mActivity.dialog != null) mActivity!!.dialog?.dismiss()
        mActivity.dialog = AlertDialogUtils.showTwoButtonDialog(
            mActivity,
            "取消",
            "我知道了",
            "你的手机没有授权软件权限,将无法正常使用!",
            R.color.color_dialog_btn,
            R.color.color_ffa300,
            object : AlertDialogUtils.DialogTwoListener {
                override fun onClickLeft(dialog: Dialog?) {
                    dialog?.dismiss()
                    showMessage("无法获取存读取权限,您的app将无法正常使用")
                    mActivity.finish()
                }

                override fun onClickRight(dialog: Dialog?) {
                    dialog?.dismiss()
                    ActivityCompat.requestPermissions(
                        mActivity,
                        needList.toTypedArray(),
                        requestCode
                    )
                }

            })
    }

    fun showMessage(message: String?) {
        if (mActivity == null || TextUtils.isEmpty(message)) return
        AlertUtil.showDeftToast(mActivity, message)
    }



}