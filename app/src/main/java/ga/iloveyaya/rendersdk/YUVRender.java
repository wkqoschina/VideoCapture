package ga.iloveyaya.rendersdk;

import android.opengl.GLSurfaceView;

import java.util.Locale;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Michael.Ma on 2016/1/28.
 */
public class YUVRender implements GLSurfaceView.Renderer {
    private static final String TAG = "YUVRender";
    private static final int INVALID_HANDLE = -1;

    private long mNativeContext = INVALID_HANDLE;
    private boolean mOpened = false;

    public YUVRender(GLSurfaceView glView) {
        mNativeContext = nativeInit(glView);
    }

    public void setVideoSize(int width, int height, int fmt) {
        nativeSetVideoSize(mNativeContext, width, height, fmt);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        LogUtils.info("onSurfaceCreated()");
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        LogUtils.info("onSurfaceChanged() " + width + " x " + height);
        nativeResize(mNativeContext, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        nativeRender(mNativeContext);
    }

    public boolean addFrame(int fmt, byte []data, int size) {
        LogUtils.debug(String.format(Locale.US, "addFrame() %d, %d", fmt, size));
        return (nativeAddFrame(mNativeContext, fmt, data, size) == 0);
    }

    public static native long nativeInit(GLSurfaceView ins);

    public static native int nativeSetVideoSize(long context, int width, int height, int fmt);

    public static native void nativeResize(long context, int width, int height);

    public static native int nativeAddFrame(long context, int fmt, byte[] data, int size);

    public static native void nativeRender(long context);

    static {
        System.loadLibrary("garender");
    }
}


