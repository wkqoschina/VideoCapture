package ga.iloveyaya.rendersdk;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

public class NativeVideoView extends GLSurfaceView {
    private static final String TAG = "NativeVideoView";
    private Context mContext;
    private YUVRender mRenderer;
    private int         mVideoWidth;
    private int         mVideoHeight;

    public NativeVideoView(Context context) {
        super(context);
        initVideoView(context);
    }

    public NativeVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initVideoView(context);
    }

    private void initVideoView(Context context) {
        mContext = context;

        setEGLContextClientVersion(2);

        mVideoWidth = 0;
        mVideoHeight = 0;
        mRenderer = new YUVRender(this);
        setRenderer(mRenderer);
        setRenderMode(RENDERMODE_WHEN_DIRTY);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
    }

    public void setVideoSize(int width, int height, int fmt) {
        mRenderer.setVideoSize(width, height, fmt);
    }

    public boolean addFrame(int fmt, byte []data, int size) {
        return mRenderer.addFrame(0, data, size);
    }
}
